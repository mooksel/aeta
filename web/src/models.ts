export enum TransactionType {
  DEBIT = 'debit',
  CREDIT = 'credit',
}

export interface Transaction {
  id: string;
  type: TransactionType;
  amount: Number;
  effectiveDate: string;
}
