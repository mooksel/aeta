import React from 'react';

import { makeStyles } from '@material-ui/core/styles';

import CircularProgress from '@material-ui/core/CircularProgress';
import AlertTitle from '@material-ui/lab/AlertTitle';
import Alert from '@material-ui/lab/Alert';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Fab from '@material-ui/core/Fab';

import AddIcon from '@material-ui/icons/Add';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';

import * as api from '../api';
import { Transaction, TransactionType } from '../models';

import { CreateTransactionDialog } from './createTransactionDialog';
import { ListItemText } from '@material-ui/core';

export const useStyles = makeStyles((theme) => ({
  addButtonFab: {
    position: 'absolute',
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  },
}));

enum TransactionsLoadingStatus {
  LOADING,
  LOADED,
  FAILED,
}

export const TransactionsList = () => {
  const classes = useStyles();

  const [loadingStatus, setloadingStatus] = React.useState<TransactionsLoadingStatus>(TransactionsLoadingStatus.LOADING);
  const [isDialogOpen, setIsDialogOpen] = React.useState<boolean>(false);
  const [transactions, setTransactions] = React.useState<Array<Transaction>>([]);

  const loadTransactions = () => {
    setloadingStatus(TransactionsLoadingStatus.LOADING);
    api.getTransactions()
      .then((resp) => {
        setTransactions(resp);
        setloadingStatus(TransactionsLoadingStatus.LOADED);
      })
      .catch(() => {
        setloadingStatus(TransactionsLoadingStatus.FAILED);
      });
  }

  React.useEffect(loadTransactions, []);


  if (loadingStatus === TransactionsLoadingStatus.LOADING) {
    return (
      <Box display='flex' alignItems='center' justifyContent='center' height='100%'>
        <CircularProgress />
      </Box>
    )
  }

  if (loadingStatus === TransactionsLoadingStatus.FAILED) {
    return (
      <Box display='flex' justifyContent='center'>
        <Alert severity='error'>
          <AlertTitle>
            {'Something went wrong :( '}
          </AlertTitle>
        </Alert>
      </Box>
    )
  }

  return (
    <Box display='flex' justifyContent='center'>
      <CreateTransactionDialog
        open={isDialogOpen}
        onClose={() => setIsDialogOpen(false)}
        onTransactionCreated={() => {
          setIsDialogOpen(false);
          loadTransactions();
        }}
      />
      {transactions.length === 0 ? (
        <Typography variant='h5' color='textSecondary'>
          {'There are no transactions yet'}
        </Typography>
      ) : (
          <List>
            {transactions.map((t, idx, arr) => {
              return (
                <ListItem divider={idx < arr.length - 1}>
                  <ListItemIcon>
                    {t.type === TransactionType.CREDIT ? <ArrowUpwardIcon /> : <ArrowDownwardIcon />}
                  </ListItemIcon>
                  <ListItemText
                    primary={`Amount: ${t.amount}`}
                    secondary={t.effectiveDate}
                  />
                </ListItem>
              )
            })}
          </List>
        )}
      <Fab
        color='primary'
        className={classes.addButtonFab}
        onClick={() => setIsDialogOpen(true)}
      >
        <AddIcon />
      </Fab>
    </Box>
  );
}
