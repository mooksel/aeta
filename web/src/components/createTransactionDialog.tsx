import React from 'react';

import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import * as api from '../api';
import { Transaction, TransactionType } from '../models';
import { Typography } from '@material-ui/core';

interface CreateTransactionDialogProps {
  open: boolean;
  onClose: () => void;
  onTransactionCreated: (transaction: Transaction) => void;
}

export const CreateTransactionDialog = (props: CreateTransactionDialogProps) => {
  const [isLoading, setIsLoading] = React.useState<boolean>(false);

  const [transactionType, setTransactionType] = React.useState<TransactionType>(TransactionType.CREDIT);
  const [transactionAmount, setTransactionAmount] = React.useState<Number>(0);
  const [transactionAmountError, setTransactionAmountError] = React.useState<null | string>(null);

  const [formError, setFormError] = React.useState<null | string>(null);

  const clearErrors = () => {
    setTransactionAmountError(null);
    setFormError(null);
  }

  const validate = () => {
    let isValid = true;

    if (!(transactionAmount > 0)) {
      isValid = false;
      setTransactionAmountError('Must be greater than 0');
    }

    return isValid;
  }

  const createTransaction = () => {
    clearErrors();
    if (validate()) {
      setIsLoading(true);
      api.createTransaction(transactionType, transactionAmount)
        .then(props.onTransactionCreated)
        .catch((resp) => {
          switch (resp.status) {
            case 409: {
              setFormError('There is not enough money on the balance')
              break;
            }
            case 422: {
              setFormError('Invalid transaction data');
              break;
            }
            default: {
              setFormError('Something went wrong')
            }
          }
        })
        .finally(() => setIsLoading(false));
    }
  }

  return (
    <Dialog
      open={props.open}
      onClose={props.onClose}
      fullWidth
    >
      <DialogTitle>
        {'Create transaction'}
      </DialogTitle>
      <DialogContent>
        {isLoading ? (
          <CircularProgress />
        ) : (
            <Grid container direction='column' spacing={2}>
              <Grid item>
                <TextField
                  variant='outlined'
                  label='amount'
                  type='number'
                  value={transactionAmount}
                  error={transactionAmountError !== null}
                  helperText={transactionAmountError}
                  onChange={(e) => setTransactionAmount(Number(e.target.value))}
                  fullWidth
                />
              </Grid>
              <Grid item>
                <TextField
                  variant='outlined'
                  label='amount'
                  value={transactionType}
                  onChange={(e) => setTransactionType(e.target.value as any as TransactionType)}
                  fullWidth
                  select
                >
                  <MenuItem value={TransactionType.CREDIT}>
                    {'Credit'}
                  </MenuItem>
                  <MenuItem value={TransactionType.DEBIT}>
                    {'Debit'}
                  </MenuItem>
                </TextField>
              </Grid>
              {formError !== null && (
                <Grid>
                  <Typography color='error' align='center'>
                    {formError}
                  </Typography>
                </Grid>
              )}
              <Grid item container justify='flex-end'>
                <Grid item>
                  <Button variant='contained' color='primary' onClick={createTransaction}>
                    {'Create'}
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          )
        }
      </DialogContent>
    </Dialog>
  )
}
