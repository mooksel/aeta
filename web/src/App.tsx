import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';

import { TransactionsList } from './components/transactionsList';

export const useStyles = makeStyles((theme) => ({
  gridContainer: {
    width: '100%',
    height: '100%',
  },
  contentGriditem: {
    flexGrow: 1,
  },
  contentContainer: {
    height: '100%',
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  }
}));

const App = () => {
  const classes = useStyles();
  return (
    <React.Fragment>
      <CssBaseline />
      <Grid container direction='column' className={classes.gridContainer}>
        <Grid item>
          <AppBar position='relative'>
            <Toolbar>
              <Typography>{'Accounting notebook'}</Typography>
            </Toolbar>
          </AppBar>
        </Grid>
        <Grid item className={classes.contentGriditem}>
          <Container className={classes.contentContainer}>
            <TransactionsList />
          </Container>
        </Grid>
      </Grid>
    </React.Fragment>
  );
};

export default App;
