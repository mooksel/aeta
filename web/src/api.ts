import { Transaction, TransactionType } from './models';

const ROOT_PATH = '/api/transactions';


export const getTransactions = async () => {
  const resp = await fetch(ROOT_PATH, {method: 'GET'});
  return await resp.json();
}


export const createTransaction = async (
  type: TransactionType,
  amount: Number,
) => {
  const resp = await fetch(ROOT_PATH, {
    method: 'POST',
    body: JSON.stringify({
      type: type,
      amount: amount,
    }),
  });

  if (resp.status >= 200 && resp.status < 300){
    return await resp.json();
  }
  throw resp
}
