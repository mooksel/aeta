# AETA - Egile Engine Test App


## How to run:

```shell
git clone git@gitlab.com:mooksel/aeta.git
cd aeta
make run
```

After executing commands above, go to http://localhost
