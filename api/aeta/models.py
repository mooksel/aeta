import enum
import datetime as dt
from dataclasses import dataclass


class TransactionType(str, enum.Enum):
    DEBIT = 'debit'
    CREDIT = 'credit'


@dataclass(frozen=True)
class Transaction():
    id: str
    type: TransactionType
    amount: int
    effective_date: dt.datetime
