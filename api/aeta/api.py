import pydantic as pd
from starlette.routing import Route
from starlette import status
from starlette.requests import Request
from starlette.responses import JSONResponse, Response

from aeta.models import TransactionType, Transaction


class CreateTransactionRequestSchema(pd.BaseModel):
    type: TransactionType
    amount: int = pd.Field(gt=0)


def _serialize_transaction(transaction: Transaction):
    return {
        'id': transaction.id,
        'type': transaction.type.value,
        'amount': transaction.amount,
        'effectiveDate': transaction.effective_date.isoformat(),
    }


async def create_transaction(request: Request):
    store = request.app.state.store
    request_json = await request.json()

    # TODO: handle invalid request payload and return 422 status code
    transaction_data = CreateTransactionRequestSchema.parse_obj(request_json)

    # NOTE: There is not necessity to make any locking mechanism fot this method because:
    # 1. Code below will be executed synchonously
    # 2. This app will be running in a single instance
    # So, there will not be any race conditions (for now)

    current_balance = store.get_balance()
    if transaction_data.type == TransactionType.DEBIT and current_balance < transaction_data.amount:
        return Response(status_code=status.HTTP_409_CONFLICT)

    if transaction_data.type == TransactionType.DEBIT:
        store.set_balance(current_balance - transaction_data.amount)
    else:
        store.set_balance(current_balance + transaction_data.amount)

    transaction = store.create_transaction(
        amount=transaction_data.amount,
        type_=transaction_data.type,
    )

    return JSONResponse(
        content=_serialize_transaction(transaction),
        status_code=status.HTTP_201_CREATED,
    )


async def get_transactions(request: Request):
    store = request.app.state.store
    transactions = store.get_transactions()
    return JSONResponse([
        _serialize_transaction(transaction)
        for transaction in transactions
    ])


async def get_transaction(request: Request):
    store = request.app.state.store
    transaction_id = request.path_params['transaction_id']
    transaction = store.get_transaction_by_id(transaction_id)
    if transaction is None:
        return Response(status_code=status.HTTP_404_NOT_FOUND)

    return JSONResponse(_serialize_transaction(transaction))


routes = [
    Route(path='/api/transactions', endpoint=create_transaction, methods=['POST']),
    Route(path='/api/transactions', endpoint=get_transactions, methods=['GET']),
    Route(path='/api/transactions/{transaction_id}', endpoint=get_transaction, methods=['GET']),
]
