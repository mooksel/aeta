from starlette.applications import Starlette
from starlette.routing import Mount

from aeta import api
from aeta import store


def app_lifespan(app: Starlette):
    # Initialize app state
    app.state.store = store.Store()
    yield
    # Clear app state


def create_app() -> Starlette:
    app = Starlette(
        routes=api.routes,
        lifespan=app_lifespan,
    )

    return app
