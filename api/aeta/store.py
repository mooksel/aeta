import typing as t
import uuid
import datetime as dt

from aeta.models import Transaction, TransactionType


class Store:

    def __init__(self):
        self._transactions = {}  # Dict[transaction_id, transaction]
        self._balance = 0

    def create_transaction(
        self,
        amount: int,
        type_: TransactionType,
    ) -> Transaction:
        transaction = Transaction(
            id=str(uuid.uuid4()),
            type=type_,
            amount=amount,
            effective_date=dt.datetime.now(),
        )

        self._transactions[transaction.id] = transaction
        return transaction

    def get_transactions(self) -> t.Sequence[Transaction]:
        return tuple(self._transactions.values())

    def get_transaction_by_id(self, transaction_id: str) -> t.Optional[Transaction]:
        return self._transactions.get(transaction_id)

    def get_balance(self) -> int:
        return self._balance

    def set_balance(self, balance: int):
        self._balance = balance
